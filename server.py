#!/usr/bin/python3
from http.server import BaseHTTPRequestHandler,HTTPServer
from cgi import parse_header, parse_multipart
from urllib.parse import parse_qsl
from urllib.parse import urlparse
from urllib.parse import urlsplit
from urllib.parse import parse_qs
from shutil import copyfile
import time
import aboutstuff
import random
import string
import index
import thankYou
import doneYet
import new
import error
import traceback



hostName="localhost"
hostPort = 8080


class MyServer(BaseHTTPRequestHandler):
    def do_GET(self):
        chalFile=open("quest.txt","r")
        lines = chalFile.read().splitlines()

        print(self.path)
        content=""
        if(self.path=="/" or self.path.startswith("/?") or self.path.startswith("/index.html")):
            nr=random.randint(0, len(lines)-1)
            try:
                par = dict(parse_qsl(urlsplit(self.path).query))
                print("p"+str(par))
                nr=int(par['id'])
                print(nr)
            except Exception as e:
                print(e)
                pass

            print(str(nr)+"of"+str(len(lines)))
            print(lines)
            challenge=lines[nr]
            text, by=challenge.split(";")
            content=index.index(questText=text,questID=nr,questName=by)
        elif(self.path.startswith("/about")or self.path.startswith("/info")or self.path.startswith("/contact")or self.path.startswith("/impressum")):
            print("aboutMe")
            content=aboutstuff.aboutstuff()
        elif(self.path.startswith("/new.html")):
            content=new.new()
        elif(self.path.startswith("/doneYet.html")):
            try:
                par = dict(parse_qsl(urlsplit(self.path).query))
                print("done"+str(par))
                nr=int(par['id'])
                challenge=lines[nr]
                text, by=challenge.split(";")
                content=doneYet.doneYet2(questText=text,questID=nr,questName=by)
            except Exception as e:
                print(traceback.format_exc())
                print(e)
                content=doneYet.doneYet()


        elif(self.path.startswith("/35c3.css")):
             f = open('35c3.css', 'r')
             content=f.read()
        elif(self.path.startswith("/favicon.ico")):
             #f = open('favicon.ico', 'r')
             content="0"


        
        else:
            print("could not find:"+self.path)
            content=error.error()
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write(bytes(content, "utf-8"))
        self.wfile.flush()
    def parse_POST(self):
       ctype, pdict = parse_header(self.headers['content-type'])
       print(ctype)
       if ctype == 'multipart/form-data':
            postvars = parse_multipart(self.rfile, pdict)
       elif ctype == 'application/x-www-form-urlencoded':
            length = int(self.headers['content-length'])
            data = self.rfile.read(length)
            print(data.decode("utf-8"));
            postvars = parse_qs(data.decode("utf-8"), encoding='utf-8', keep_blank_values=1)
       else:
            postvars = {}
       return postvars
    def do_POST(self):
        print("p"+self.path)
        if(self.path.startswith("/thankYou.html")):
            postvars = self.parse_POST()
            print(postvars)
            self.send_response(200)
            futurFile=open("future.txt","a")
            try:
            	futurFile.write(postvars['text'][0]+";"+postvars['by'][0]+"\n")
            except Exception as e:
                print(e)
                try:
                    futurFile.write(postvars['text'][0]+";"+"anonymous"+"\n")
                except Exception as e:
                    print(e)

            self.send_header("content-type", "text/html")
            self.end_headers()
            self.wfile.write(bytes(thankYou.thankYou(), "utf-8"))
            self.wfile.flush()

            print("done")

    def do_HEAD(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

myServer = HTTPServer((hostName, hostPort), MyServer)
print(time.asctime(), "Server Starts - %s:%s" % (hostName, hostPort))

try:
    myServer.serve_forever()
except KeyboardInterrupt:
    pass

myServer.server_close()
print(time.asctime(), "Server Stops - %s:%s" % (hostName, hostPort))
