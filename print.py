#!/usr/bin/python3.5 
import subprocess
from random import SystemRandom
import textwrap
from io import StringIO
from unittest import mock
import qrcode
import sys
try:
    import RPi.GPIO as GPIO
    GPIO.setup(23, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.add_event_detect(17, GPIO.FALLING, callback=my_callback, bouncetime=300)  
# GPIO 23 set up as inputs, pulled up to avoid false detection.  
# Both ports are wired to connect to GND on button press.  
# So we'll be setting up falling edge detection for both  
    GPIO.setup(23, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.add_event_detect(23, GPIO.FALLING, callback=my_callback, bouncetime=300)  
except:
    print("not on a raspi")

def my_callback(channel):  
    print( "falling edge detected on 23"  )
    printing()

def printing():
    cryptogen = SystemRandom()

    lpr =  subprocess.Popen(["lpr","-P", "Metapace_T-3II"] , stdin=subprocess.PIPE)
    header=open("header","r")
    lines = header.read()
    lpr.stdin.write(lines.encode('ascii'))
    chalFile=open("quest.txt","r")
    lines = chalFile.read().splitlines()
    nr=cryptogen.randint(0, len(lines)-1)
    challenge=lines[nr]
    text, by=challenge.split(";")

    lpr.stdin.write(bytes(textwrap.fill(text,20),'utf-8'))#.encod('utf-8'))
    lpr.stdin.write(bytes("\n by:"+by,'utf-8'))#.encod('utf-8'))
    lpr.stdin.write(bytes("\n\nA service by c3quest.fun3\n\n",'utf-8'))#.encod('utf-8'))
    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_L,
        box_size=2,
        border=0,
    )
    buf = StringIO()
    qr.add_data('https://c3quest.fun/?id='+str(nr)+'')
    qr.make(fit=True)

    qr.print_ascii(tty=False, out=buf)

    lpr.stdin.write(bytes(buf.getvalue(), 'utf-8' ))
    lpr.stdin.close()
